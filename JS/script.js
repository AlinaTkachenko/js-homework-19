function calculateAccomplish(arrTeamMember, arrTask, deadline) {
     //Скільки всього може виконати сторі поінтів команда за день.
     let sumStoryPointOfTeamMember = arrTeamMember.reduce((sum, current) => sum + current, 0);
     //Скільки всього потрібно виконати сторі поінтів.
     let sumStoryPointOfTask = arrTask.reduce((sum, current) => sum + current, 0);
     //Скільки потрібно днів команді ,щоб виконати завдання.
     let timePerform = sumStoryPointOfTask / sumStoryPointOfTeamMember;

     let workDaysToDeadline = 1;
     let nowDate = new Date();
     let dateOfNowDate = nowDate.getDate();
     while(nowDate.getDate() !== deadline.getDate() || nowDate.getMonth() !== deadline.getMonth()){
         
          if(nowDate.getDate() === 1){
               dateOfNowDate = 1;
          }
          dateOfNowDate = dateOfNowDate + 1;
          nowDate.setDate(dateOfNowDate);

          if (nowDate.getDay() !== 0 && nowDate.getDay() < 6) {
               workDaysToDeadline++;
          }    
     }
     

     if (workDaysToDeadline > timePerform) {
          let extraDays = workDaysToDeadline - timePerform;
          alert(`Усі завдання будуть успішно виконані за ${Math.floor(extraDays)} днів до настання дедлайну!`);
     }else{
          let hoursInDay = 8;
          let hoursForTask = timePerform * hoursInDay;
          let hoursWeHave = workDaysToDeadline * hoursInDay;
          let hoursExtra = hoursForTask - hoursWeHave;

          alert(`Команді розробників доведеться витратити додатково ${Math.ceil(hoursExtra)}годин після дедлайну, щоб виконати всі завдання в беклозі.`);  
     }

}

var deadline = new Date(2023, 4, 29);
calculateAccomplish([5, 3, 2], [5, 10, 28, 60], deadline);